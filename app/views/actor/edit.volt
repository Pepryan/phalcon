{% extends 'template/index.volt' %}
{% block content %}
{{ link_to("actor", "&larr; Data Actor", "class": "btn btn-default") }}

<h3><i class="fa fa-pencil"></i> Edit Actor</h3>
<!-- Show Flash Session Data -->
<p>{{flashSession.output()}}</p>

{{ form("actor/save", "role": "form") }}
<div class="form-group"><input type="hidden" name="act_id" value="<?php echo $act_id; ?>"> </div>
<div class="form-group">
    <label for="act_fname">Actor First Name</label>
    <input type="text" name="act_fname" class="form-control" value="<?php echo $act_fname ?>">
</div>

<div class="form-group">
    <label for="act_lname">Actor Last Name</label>
    <input type="text" name="act_lname" class="form-control" value="<?php echo $act_lname ?>">
</div>

<div class="form-group">
    <label class="control-label">Actor Gender</label>
    <select name="act_gender" class="form-control">
        <option value="">--Select Gender--</option>
        <option value="M" <?php if($act_gender == "M"){?> selected="selected" <?php }?>>Male</option>
        <option value="F" <?php if($act_gender == "F"){?> selected="selected" <?php }?>>Female</option>
    </select>
</div>
<div class="form-group">{{ submit_button("Simpan", "class": "btn btn-primary") }}</div>
</form>
{% endblock %}