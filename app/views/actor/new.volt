{% extends 'template/index.volt' %}
{% block content %}
{{ link_to("actor", "&larr; Data Actor", "class": "btn btn-default") }}
<p>
    <center><b>{{flashSession.output()}}</b></center>
</p>
<div class="center scaffold">
    <h2>Tambah Actor Baru</h2>
    {{ form("actor/create", "role": "form", "class":"") }}
    <div class="form-group">
        <input type="hidden" name="act_id"></div>
    <div class="form-group">
        <label for="act_fname">Actor First Name</label>
        {{ text_field("act_fname", "class":"form-control","placeholder": "First Name", "size":"20") }}
    </div>
    <div class="form-group">
        <label for="act_lname">Actor Last Name</label>
        {{ text_field("act_lname", "class":"form-control", "placeholder": "Last Name", "size":"20") }}
    </div>
    <div class="form-group">
        <label class="control-label">Actor Gender</label>
        <select name="act_gender" class="form-control">
            <option value="">--Select Gender--</option>
            <option value="M">Male</option>
            <option value="F">Female</option>
        </select>
    </div>
    <div class="form-group">{{ submit_button("Simpan", "class": "btn btn-success") }}</div>
    </form>
</div>
<hr>
{% endblock %}