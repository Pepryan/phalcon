{% extends 'template/index.volt' %}
{% block content %}
<h1>Data Actor</h1>
<center>
    <div class="row">
        <div class="col-md-2">
            {{ link_to("actor/new", "Tambah Actor", "class": "btn btn-success") }}
        </div>
        <div class="col-md-10">
            {{ form("actor/search", "role": "form", "class":"form-inline", "autocomplete":"off") }}
            <div class="form-group">
                <label>Search Actor :</label>
                {{ numeric_field("act_id", "class":"form-control", "placeholder": "Cari ID", "size":"10") }}</div>
            <div class="form-group">
                {{ text_field("act_fname", "class":"form-control","placeholder": "Cari First Name", "size":"20") }}
            </div>
            <div class="form-group">
                {{ text_field("act_lname", "class":"form-control", "placeholder": "Cari Last Name", "size":"20") }}
            </div>
            <div class="form-group">{{ submit_button("Cari", "class": "btn btn-primary") }}</div>
            </form>
        </div>
    </div>
</center>
<hr>
<center><b>{{flashSession.output()}}</b></center>
{% for actor in page.items %}
{% if loop.first %}
<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Actor ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Gender</th>
            <th style="width:250px;">Action</th>
        </tr>
    </thead>
    <tbody>
        {% endif %}
        <tr>
            <td>{{ actor.act_id }}</td>
            <td>{{ actor.act_fname }}</td>
            <td>{{ actor.act_lname }}</td>
            <td>{{ actor.act_gender }}</td>
            <td>
                {{ link_to("actor/edit/" ~ actor.act_id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-info") }}
                {{ link_to("actor/delete/" ~ actor.act_id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-danger", "onclick":"return confirm('Apakah anda akan menghapus data ? ');") }}
            </td>
        </tr>
        {% if loop.last %}
    </tbody>
    <tfoot>
        <tr>
            <th>Actor ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Gender</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
<div align="right">
    {{ link_to("actor/index", ' First', "class": "btn btn-default") }}
    {{ link_to("actor/index?page=" ~ page.before, '&laquo;Previous', "class": "btn btn-default") }}
    {{ link_to("actor/index?page=" ~ page.next, 'Next&raquo;', "class": "btn btn-default") }}
    {{ link_to("actor/index?page=" ~ page.last, 'Last', "class": "btn btn-default") }}
    Page :<span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span></div>
</div>
{% endif %}
{% else %}
Actor Tidak ditemukan
{% endfor %}
<hr>

{% endblock %}