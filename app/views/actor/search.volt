{% extends 'template/index.volt' %}
{% block content %}
<div class="container" style="padding-left:40px;padding-right: 20px;">
    <center>
        <div class="page-header">
            <h1> Hasil Pencarian</h1>
        </div>
    </center>
    {{ link_to("actor", "&larr; Kembali") }}
    {% for data in datas %}
    {% if loop.first %}
    <table id="table" class="table table-striped table-bordered" cellspacing="0" align="center">
        <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        {% endif %}
        <tbody>
            <tr>
                <td>{{ data.act_id }}</td>
                <td>{{ data.act_fname }}</td>
                <td>{{ data.act_lname }}</td>
                <td>{{ data.act_gender }}</td>
                <td> <a href="{{ url('actor/edit/' ~ data.act_id) }}">Edit</a> </td>
                <td> <a href="{{ url('actor/delete/' ~ data.act_id) }}"
                        onclick="return confirm('Apakah anda akan menghapus data ? ');">Hapus</a> </td>
            </tr>
        </tbody>
        {% if loop.last %}
    </table>
    {% endif %}
    {% else %}
    No data
    {% endfor %}
</div>
{% endblock %}