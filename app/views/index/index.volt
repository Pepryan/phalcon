{% extends 'template/index.volt' %}

{% block content %}
<div style="padding-left: 10%;">
    <div class="page-header">
        <h1>Congratulations!</h1>
    </div>

    <p>You're now flying with Phalcon. Great things are about to happen!</p>

    <p>This page is located at <code>views/index/index.phtml</code></p>

    <h1>
        <a href="{{ url('actor') }}">Go to Actor table</a>
    </h1>
</div>
{% endblock %}