<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\InclusionIn;

class Actor extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $act_id;

    /**
     *
     * @var string
     */
    public $act_fname;

    /**
     *
     * @var string
     */
    public $act_lname;

    /**
     *
     * @var string
     */
    public $act_gender;
    public function initialize()
    {
        $this->setSchema("movies_ob19");
        $this->setSource("actor");
    }
    public function getSource()
    {
        return 'actor';
    }
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'act_id',
            new Uniqueness(
                [
                    'message' => 'ID actor sudah ada',
                ]
            )
        );

        return $this->validate($validator);
    }
}
