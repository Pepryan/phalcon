<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Flash\Direct as FlashDirect;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ActorController extends ControllerBase
{

    /**
     * The start action, it shows the "search" view
     */
    public function indexAction()
    {
        // $this->persistent->parameters = null;
        $this->view->setMainView('index');
        $numberPage = 1;
        $numberPage = $this->request->getQuery("page", "int");
        $actor = Actor::find(array('order' => 'act_id DESC'));
        $this->view->data = $actor;
        $paginator = new Paginator([
            'data' => $actor,
            'limit' => 5,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Execute the "search" based on the criteria sent from the "index"
     * Returning a paginator for the results
     */
    public function searchAction()
    {

        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Actor", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $actor = Actor::find($parameters);
        $this->view->datas = $actor;
    }

    /**
     * Shows the view to create a "new" product
     */
    public function newAction()
    {
        // ...
    }

    /**
     * Shows the view to "edit" an existing product
     */
    public function editAction($act_id)
    {
        $actor = Actor::findFirstByAct_id($act_id);
        $this->view->act_id = $actor->act_id;
        $this->view->act_fname = $actor->act_fname;
        $this->view->act_lname = $actor->act_lname;
        $this->view->act_gender = $actor->act_gender;

        if (!$this->request->isPost()) {

            $actor = Actor::findFirstByAct_id($act_id);
            if (!$actor) {
                $this->flashSession->error("Actor tidak ditemukan");

                $this->dispatcher->forward([
                    'controller' => "actor",
                    'action' => 'index'
                ]);

                return;
            }
        }
    }

    /**
     * Creates a product based on the data entered in the "new" action
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "actor",
                'action' => 'index'
            ]);

            return;
        }

        $actor = new Actor();
        $actor->act_id = $this->request->getPost("act_id");
        $actor->act_fname = $this->request->getPost("act_fname");
        $actor->act_lname = $this->request->getPost("act_lname");
        $actor->act_gender = $this->request->getPost("act_gender");


        if (!$actor->save()) {
            foreach ($actor->getMessages() as $message) {
                $this->flashSession->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "actor",
                'action' => 'new'
            ]);

            return;
        }

        $this->flashSession->success("Actor berhasil disimpan");

        $this->dispatcher->forward([
            'controller' => "actor",
            'action' => 'index'
        ]);
    }

    /**
     * Updates a product based on the data entered in the "edit" action
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "actor",
                'action' => 'index'
            ]);

            return;
        }

        $act_id = $this->request->getPost("act_id");
        $actor = Actor::findFirstByAct_id($act_id);

        if (!$actor) {
            $this->flashSession->error("Actor tidak ada " . $act_id);

            $this->dispatcher->forward([
                'controller' => "actor",
                'action' => 'index'
            ]);

            return;
        }

        $actor->act_id = $this->request->getPost("act_id");
        $actor->act_fname = $this->request->getPost("act_fname");
        $actor->act_lname = $this->request->getPost("act_lname");
        $actor->act_gender = $this->request->getPost("act_gender");


        if (!$actor->save()) {

            foreach ($actor->getMessages() as $message) {
                $this->flashSession->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "actor",
                'action' => 'edit',
                'params' => [$actor->act_id]
            ]);

            return;
        }

        $this->flashSession->success("Actor berhasil diupdate");

        $this->dispatcher->forward([
            'controller' => "actor",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes an existing product
     */
    public function deleteAction($act_id)
    {
        $actor = Actor::findFirstByAct_id($act_id);
        if (!$actor) {
            $this->flashSession->error("Actor tidak ditemukan");

            $this->dispatcher->forward([
                'controller' => 'actor',
                'action' => 'index'
            ]);

            return;
        }

        if (!$actor->delete()) {

            foreach ($actor->getMessages() as $message) {
                $this->flashSession->error($message);
            }

            $this->dispatcher->forward([
                'controller' => 'actor',
                'action' => 'index'
            ]);

            return;
        }

        $this->flashSession->success("Actor berhasil dihapus");

        $this->dispatcher->forward([
            'controller' => "actor",
            'action' => "index"
        ]);
    }
}
