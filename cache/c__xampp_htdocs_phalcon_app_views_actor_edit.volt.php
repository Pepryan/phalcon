<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= $this->url->get('') ?>">PT. Ciptadrasoft</a>
			</div>
		</div>
	</nav>
	<div class="container">
		
<?= $this->tag->linkTo(['actor', '&larr; Data Actor', 'class' => 'btn btn-default']) ?>

<h3><i class="fa fa-pencil"></i> Edit Actor</h3>
<!-- Show Flash Session Data -->
<p><?= $this->flashSession->output() ?></p>

<?= $this->tag->form(['actor/save', 'role' => 'form']) ?>
<div class="form-group"><input type="hidden" name="act_id" value="<?php echo $act_id; ?>"> </div>
<div class="form-group">
    <label for="act_fname">Actor First Name</label>
    <input type="text" name="act_fname" class="form-control" value="<?php echo $act_fname ?>">
</div>

<div class="form-group">
    <label for="act_lname">Actor Last Name</label>
    <input type="text" name="act_lname" class="form-control" value="<?php echo $act_lname ?>">
</div>

<div class="form-group">
    <label class="control-label">Actor Gender</label>
    <select name="act_gender" class="form-control">
        <option value="">--Select Gender--</option>
        <option value="M" <?php if($act_gender == "M"){?> selected="selected" <?php }?>>Male</option>
        <option value="F" <?php if($act_gender == "F"){?> selected="selected" <?php }?>>Female</option>
    </select>
</div>
<div class="form-group"><?= $this->tag->submitButton(['Simpan', 'class' => 'btn btn-primary']) ?></div>
</form>

	</div>
</body>

</html>