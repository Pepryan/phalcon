<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= $this->url->get('') ?>">PT. Ciptadrasoft</a>
			</div>
		</div>
	</nav>
	<div class="container">
		
<?= $this->tag->linkTo(['actor', '&larr; Data Actor', 'class' => 'btn btn-default']) ?>
<p>
    <center><b><?= $this->flashSession->output() ?></b></center>
</p>
<div class="center scaffold">
    <h2>Tambah Actor Baru</h2>
    <?= $this->tag->form(['actor/create', 'role' => 'form', 'class' => '']) ?>
    <div class="form-group">
        <input type="hidden" name="act_id"></div>
    <div class="form-group">
        <label for="act_fname">Actor First Name</label>
        <?= $this->tag->textField(['act_fname', 'class' => 'form-control', 'placeholder' => 'First Name', 'size' => '20']) ?>
    </div>
    <div class="form-group">
        <label for="act_lname">Actor Last Name</label>
        <?= $this->tag->textField(['act_lname', 'class' => 'form-control', 'placeholder' => 'Last Name', 'size' => '20']) ?>
    </div>
    <div class="form-group">
        <label class="control-label">Actor Gender</label>
        <select name="act_gender" class="form-control">
            <option value="">--Select Gender--</option>
            <option value="M">Male</option>
            <option value="F">Female</option>
        </select>
    </div>
    <div class="form-group"><?= $this->tag->submitButton(['Simpan', 'class' => 'btn btn-success']) ?></div>
    </form>
</div>
<hr>

	</div>
</body>

</html>