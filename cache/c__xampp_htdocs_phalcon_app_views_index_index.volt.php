<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= $this->url->get('') ?>">PT. Ciptadrasoft</a>
			</div>
		</div>
	</nav>
	<div class="container">
		
<div style="padding-left: 10%;">
    <div class="page-header">
        <h1>Congratulations!</h1>
    </div>

    <p>You're now flying with Phalcon. Great things are about to happen!</p>

    <p>This page is located at <code>views/index/index.phtml</code></p>

    <h1>
        <a href="<?= $this->url->get('actor') ?>">Go to Actor table</a>
    </h1>
</div>

	</div>
</body>

</html>