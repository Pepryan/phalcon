<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= $this->url->get('') ?>">PT. Ciptadrasoft</a>
			</div>
		</div>
	</nav>
	<div class="container">
		
<h1>Data Actor</h1>
<center>
    <div class="row">
        <div class="col-md-2">
            <?= $this->tag->linkTo(['actor/new', 'Tambah Actor', 'class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-10">
            <?= $this->tag->form(['actor/search', 'role' => 'form', 'class' => 'form-inline', 'autocomplete' => 'off']) ?>
            <div class="form-group">
                <label>Search Actor :</label>
                <?= $this->tag->numericField(['act_id', 'class' => 'form-control', 'placeholder' => 'Cari ID', 'size' => '10']) ?></div>
            <div class="form-group">
                <?= $this->tag->textField(['act_fname', 'class' => 'form-control', 'placeholder' => 'Cari First Name', 'size' => '20']) ?>
            </div>
            <div class="form-group">
                <?= $this->tag->textField(['act_lname', 'class' => 'form-control', 'placeholder' => 'Cari Last Name', 'size' => '20']) ?>
            </div>
            <div class="form-group"><?= $this->tag->submitButton(['Cari', 'class' => 'btn btn-primary']) ?></div>
            </form>
        </div>
    </div>
</center>
<hr>
<center><b><?= $this->flashSession->output() ?></b></center>
<?php $v40371084331iterated = false; ?><?php $v40371084331iterator = $page->items; $v40371084331incr = 0; $v40371084331loop = new stdClass(); $v40371084331loop->self = &$v40371084331loop; $v40371084331loop->length = count($v40371084331iterator); $v40371084331loop->index = 1; $v40371084331loop->index0 = 1; $v40371084331loop->revindex = $v40371084331loop->length; $v40371084331loop->revindex0 = $v40371084331loop->length - 1; ?><?php foreach ($v40371084331iterator as $actor) { ?><?php $v40371084331loop->first = ($v40371084331incr == 0); $v40371084331loop->index = $v40371084331incr + 1; $v40371084331loop->index0 = $v40371084331incr; $v40371084331loop->revindex = $v40371084331loop->length - $v40371084331incr; $v40371084331loop->revindex0 = $v40371084331loop->length - ($v40371084331incr + 1); $v40371084331loop->last = ($v40371084331incr == ($v40371084331loop->length - 1)); ?><?php $v40371084331iterated = true; ?>
<?php if ($v40371084331loop->first) { ?>
<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Actor ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Gender</th>
            <th style="width:250px;">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php } ?>
        <tr>
            <td><?= $actor->act_id ?></td>
            <td><?= $actor->act_fname ?></td>
            <td><?= $actor->act_lname ?></td>
            <td><?= $actor->act_gender ?></td>
            <td>
                <?= $this->tag->linkTo(['actor/edit/' . $actor->act_id, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class' => 'btn btn-info']) ?>
                <?= $this->tag->linkTo(['actor/delete/' . $actor->act_id, '<i class="glyphicon glyphicon-remove"></i> Delete', 'class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Apakah anda akan menghapus data ? \');']) ?>
            </td>
        </tr>
        <?php if ($v40371084331loop->last) { ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Actor ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Gender</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
<div align="right">
    <?= $this->tag->linkTo(['actor/index', ' First', 'class' => 'btn btn-default']) ?>
    <?= $this->tag->linkTo(['actor/index?page=' . $page->before, '&laquo;Previous', 'class' => 'btn btn-default']) ?>
    <?= $this->tag->linkTo(['actor/index?page=' . $page->next, 'Next&raquo;', 'class' => 'btn btn-default']) ?>
    <?= $this->tag->linkTo(['actor/index?page=' . $page->last, 'Last', 'class' => 'btn btn-default']) ?>
    Page :<span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span></div>
</div>
<?php } ?>
<?php $v40371084331incr++; } if (!$v40371084331iterated) { ?>
Actor Tidak ditemukan
<?php } ?>
<hr>


	</div>
</body>

</html>