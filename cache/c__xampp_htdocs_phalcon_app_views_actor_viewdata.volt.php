<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	

<div class="container" style="padding-left:40px;padding-right: 20px;">
    <div class="page-header">
        <h1>Ini halaman actor</h1>
    </div>
    <h3>Actor Data</h3>
    <br />
    <center>
        <?= $this->tag->form(['actor/search', 'role' => 'form']) ?>
        <?= $this->tag->numericField(['act_id', 'placeholder' => 'Search ID', 'class' => 'form-control']) ?>
        <?= $this->tag->textField(['act_fname', 'placeholder' => 'Search First Name', 'class' => 'form-control']) ?>
        <?= $this->tag->textField(['act_lname', 'placeholder' => 'Search Last Name', 'class' => 'form-control']) ?>
        <?= $this->tag->submitButton(['Cari', 'class' => 'btn btn-primary']) ?>
        </form>
    </center>
    <br>
    <?php $v31472910221iterated = false; ?><?php $v31472910221iterator = $page->items; $v31472910221incr = 0; $v31472910221loop = new stdClass(); $v31472910221loop->self = &$v31472910221loop; $v31472910221loop->length = count($v31472910221iterator); $v31472910221loop->index = 1; $v31472910221loop->index0 = 1; $v31472910221loop->revindex = $v31472910221loop->length; $v31472910221loop->revindex0 = $v31472910221loop->length - 1; ?><?php foreach ($v31472910221iterator as $actor) { ?><?php $v31472910221loop->first = ($v31472910221incr == 0); $v31472910221loop->index = $v31472910221incr + 1; $v31472910221loop->index0 = $v31472910221incr; $v31472910221loop->revindex = $v31472910221loop->length - $v31472910221incr; $v31472910221loop->revindex0 = $v31472910221loop->length - ($v31472910221incr + 1); $v31472910221loop->last = ($v31472910221incr == ($v31472910221loop->length - 1)); ?><?php $v31472910221iterated = true; ?>
    <?php if ($v31472910221loop->first) { ?>
    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Actor ID</th>
                <th>Actor First Name</th>
                <th>Actor Last Name</th>
                <th>Actor Gender</th>
                <th style="width:250px;">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php } ?>
            <tr>
                <td><?= $actor->act_id ?></td>
                <td><?= $actor->act_fname ?></td>
                <td><?= $actor->act_lname ?></td>
                <td><?= $actor->act_gender ?></td>
                <td>
                    <?= $this->tag->linkTo(['actor/edit/' . $actor->act_id, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class' => 'btn btn-info']) ?>
                    &nbsp;
                    <?= $this->tag->linkTo(['actor/delete/' . $actor->act_id, '<i class="glyphicon glyphicon-remove"></i> Delete', 'class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Apakah anda akan menghapus data ? \');']) ?>
                </td>
            </tr>
            <?php if ($v31472910221loop->last) { ?>
        </tbody>

        <tfoot>
            <tr>
                <th>Actor ID</th>
                <th>Actor First Name</th>
                <th>Actor Last Name</th>
                <th>Actor Gender</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    <div colspan="4" align="right">
        <div class="btn-group">
            <?= $this->tag->linkTo(['actor/viewData', ' First', 'class' => 'btn btn-default']) ?>
            <?= $this->tag->linkTo(['actor/viewData?page=' . $page->before, ' Previous', 'class' => 'btn btn-default']) ?>
            <?= $this->tag->linkTo(['actor/viewData?page=' . $page->next, ' Next', 'class' => 'btn btn-default']) ?>
            <?= $this->tag->linkTo(['actor/viewData?page=' . $page->last, ' Last', 'class' => 'btn btn-default']) ?>
            <p>Page :<span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span></p>
        </div>

        <?php } ?>
        <?php $v31472910221incr++; } if (!$v31472910221iterated) { ?>
        Actor Tidak ditemukan
        <?php } ?>
        <hr>
    </div>
</div>

</body>

</html>