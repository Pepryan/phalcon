<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>Phalcon PT. Ciptadrasoft</title>
	<?= $this->tag->stylesheetLink('bootstrap/css/bootstrap.min.css') ?>
	<?= $this->tag->stylesheetLink('datatables/css/dataTables.bootstrap.css') ?>
	<?= $this->tag->javascriptInclude('jquery/jquery.min.js') ?>
	<?= $this->tag->javascriptInclude('bootstrap/js/bootstrap.min.js') ?>
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= $this->url->get('') ?>">PT. Ciptadrasoft</a>
			</div>
		</div>
	</nav>
	<div class="container">
		
<div class="container" style="padding-left:40px;padding-right: 20px;">
    <center>
        <div class="page-header">
            <h1> Hasil Pencarian</h1>
        </div>
    </center>
    <?= $this->tag->linkTo(['actor', '&larr; Kembali']) ?>
    <?php $v22161107671iterated = false; ?><?php $v22161107671iterator = $datas; $v22161107671incr = 0; $v22161107671loop = new stdClass(); $v22161107671loop->self = &$v22161107671loop; $v22161107671loop->length = count($v22161107671iterator); $v22161107671loop->index = 1; $v22161107671loop->index0 = 1; $v22161107671loop->revindex = $v22161107671loop->length; $v22161107671loop->revindex0 = $v22161107671loop->length - 1; ?><?php foreach ($v22161107671iterator as $data) { ?><?php $v22161107671loop->first = ($v22161107671incr == 0); $v22161107671loop->index = $v22161107671incr + 1; $v22161107671loop->index0 = $v22161107671incr; $v22161107671loop->revindex = $v22161107671loop->length - $v22161107671incr; $v22161107671loop->revindex0 = $v22161107671loop->length - ($v22161107671incr + 1); $v22161107671loop->last = ($v22161107671incr == ($v22161107671loop->length - 1)); ?><?php $v22161107671iterated = true; ?>
    <?php if ($v22161107671loop->first) { ?>
    <table id="table" class="table table-striped table-bordered" cellspacing="0" align="center">
        <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <?php } ?>
        <tbody>
            <tr>
                <td><?= $data->act_id ?></td>
                <td><?= $data->act_fname ?></td>
                <td><?= $data->act_lname ?></td>
                <td><?= $data->act_gender ?></td>
                <td> <a href="<?= $this->url->get('actor/edit/' . $data->act_id) ?>">Edit</a> </td>
                <td> <a href="<?= $this->url->get('actor/delete/' . $data->act_id) ?>"
                        onclick="return confirm('Apakah anda akan menghapus data ? ');">Hapus</a> </td>
            </tr>
        </tbody>
        <?php if ($v22161107671loop->last) { ?>
    </table>
    <?php } ?>
    <?php $v22161107671incr++; } if (!$v22161107671iterated) { ?>
    No data
    <?php } ?>
</div>

	</div>
</body>

</html>